/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ["**/**/*.test.ts"],
  verbose: true,
  setupFilesAfterEnv: ["<rootDir>/__tests__/setupTests.ts"],
  forceExit: true
};