import InMemoryRepository from '../../../src/repositories/signature/inMemoryRepository';
import {NotFoundError} from "../../../src/api/errors";



describe('inMemoryRepository', () => {
    let repository: InMemoryRepository;

    beforeEach(() => {
        repository = new InMemoryRepository();
    });

    describe('#create', () => {
        it('creates a signature', async () => {
            const signature: string = "sdfsdfsdfsdfdf";
            const createdSignature = await repository.create(signature);
            expect(createdSignature).toEqual(signature);
        });
    });

    describe('#findLast', () => {
        it('returns undefined if signature not found',  async () => {
            await expect(async () => {
                await repository.findLast();
            }).rejects.toThrowError
        });

        it('returns the last created signature if found', async () => {
            const signature: string = "sdfsdfsdfsdfdf";
            await repository.create(signature);
            const lastSignature = await repository.findLast();
            expect(lastSignature).toEqual(signature);
        });
    });
});