import InMemoryRepository from '../../../src/repositories/device/inMemoryRepository';
import Device from '../../../src/models/device';


const id = "4b1d9c23-5f26-4a75-a361-2cf81bf3bed7"
jest.mock('uuid', () => ({
    v4: jest.fn(() => id),
}));

describe('inMemoryRepository', () => {
    let repository: InMemoryRepository = new InMemoryRepository();


    describe('#create', () => {
        it('creates a device', async () => {
            const device: Device = {
                id: id,
                algorithm: 'RSA',
                label: 'Device 1',
                public_key: id,
                private_key: id,
                signature_counter: 0
            };
            await repository.create(device);
            const result = await repository.find();
            expect(result).toEqual(device);
        });
    });

    describe('#find', () => {
        it('returns undefined if device not found', async () => {
            await expect(async () => {
                await repository.find();
            }).rejects.toThrowError
        });

        it('returns the device if found', async () => {
            const device: Device = {
                id: id,
                algorithm: 'RSA',
                label: 'Device 1',
                public_key: id,
                private_key: id,
                signature_counter: 0
            };
            await repository.create(device);
            const result = await repository.find();
            expect(result).toEqual(device);
        });
    });

    describe('#incrementCounter', () => {
        it('increment counter of the device', async () => {
            const device: Device = {
                id: id,
                algorithm: 'RSA',
                label: 'Device 1',
                public_key: id,
                private_key: id,
                signature_counter: 0
            };
            await repository.create(device);
            const result = await repository.incrementCounter();
            expect(result.signature_counter).toEqual(1);
        });

        it("raise exception when there is no device", async() => {
            await expect(async () => {
                await repository.incrementCounter();
            }).rejects.toThrowError
        })
    });
});