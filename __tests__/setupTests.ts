import fs from "fs";
import path from "path";

// this is to purge the data before and after running each unit test it should change depending on the data persistent logic

beforeEach(() => {
    deleteAllDataFiles('./src/repositories/device')
    deleteAllDataFiles('./src/repositories/signature')

});

afterEach(() => {
    deleteAllDataFiles('./src/repositories/device')
    deleteAllDataFiles('./src/repositories/signature')
})

function deleteAllDataFiles(directoryPath: string): void {
    const files = fs.readdirSync(path.resolve(directoryPath));
    for (const file of files) {
        const filePath = path.join(directoryPath, file);

        // Check if the file extension is .txt or .json before deleting
        if (['.txt', '.json'].includes(path.extname(filePath))) {
            fs.unlinkSync(filePath);
        }
    }
}