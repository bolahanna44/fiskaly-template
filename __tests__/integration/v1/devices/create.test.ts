import request from 'supertest';
import server from '../../../../src/api/server'
import {DeviceCreateRequest} from "../../../../src/api/v1/schemas/requests/createDevice";

const id = "4b1d9c23-5f26-4a75-a361-2cf81bf3bed7"
jest.mock('uuid', () => ({
    v4: jest.fn(() => id),
}));

describe("device", () => {
    describe("create", () => {
        const URL = '/api/v1/devices'

        describe("with wrong request body", () => {
            it("returns 400 status code", async() => {
                const requestBody = {
                    "data": {
                        "type": "photos",
                        "attributes": {
                            "title": "fiskaly"
                        },
                    }
                }
                const response = await request(server).post(URL).send(requestBody);
                expect(response.status).toBe(400);
            })
        })

        describe("with the correct request body", () => {
            const requestBody: DeviceCreateRequest = {
                "data": {
                    "type": "devices",
                    "attributes": {
                        "label": "fiskaly",
                        "algorithm": 'ECA'
                    },
                }
            }

            it("responds with 201 status code", async() => {
                const response = await request(server).post(URL).send(requestBody);
                expect(response.status).toBe(201);
            })

            it("responds with the created device resource", async() => {
                const response = await request(server).post(URL).send(requestBody);
                expect(response.body).toEqual({data: { ...requestBody.data, id: id } })
            })

            // we can test response schema using approvals package
        })


    })
})


