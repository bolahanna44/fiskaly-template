import request from 'supertest';
import server from '../../../../src/api/server'
import SignTransactionResponseSchema from "../../../../src/api/v1/schemas/responses/signTransaction";
import Ajv from "ajv";
import deviceInMemory from "../../../../src/repositories/device/inMemoryRepository";
import Device from "../../../../src/models/device";

describe("device", () => {
    describe("sign_transaction", () => {
        const URL = `/api/v1/devices/sign_transaction`
        const createdDevice: Device = {
            id: "adsf",
            signature_counter: 0,
            public_key: "dsfds",
            private_key: "sdfsdf",
            algorithm: "ECA"
        }

        beforeEach(() => {
            jest.spyOn(deviceInMemory.prototype, 'find').mockResolvedValue(createdDevice)
            jest.spyOn(deviceInMemory.prototype, 'incrementCounter').mockResolvedValue(
                {...createdDevice, signature_counter: createdDevice.signature_counter + 1}
            )
        })

        describe("with wrong request body", () => {
            it("returns 400 status code", async() => {
                const requestBody = {
                    "wrong_data": "data_to_sign"
                }
                const response = await request(server).post(URL).send(requestBody);
                expect(response.status).toEqual(400);
            })
        })

        describe("with the correct request body", () => {
            const requestBody = {
                "data": "data_to_sign"
            }

            it("responds with 200 status code", async() => {
                const response = await request(server).post(URL).send(requestBody);
                expect(response.status).toEqual(200);
            })

            it("responds with the correct data", async() => {
                // we can use approvals package to test the response schema

                const response = await request(server).post(URL).send(requestBody);
                const ajv = new Ajv()
                const isValidResponse = ajv.validate(SignTransactionResponseSchema, response.body);

                expect(isValidResponse).toBe(true)
            })
        })

    })
})