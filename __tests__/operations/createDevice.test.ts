import Device from '../../src/models/device';
import CreateDeviceOperation from '../../src/operations/createDeviceOperation';
import {DeviceCreateRequest} from "../../src/api/v1/schemas/requests/createDevice";
import InMemoryRepository from "../../src/repositories/device/inMemoryRepository";

const id = "4b1d9c23-5f26-4a75-a361-2cf81bf3bed7"
jest.mock('uuid', () => ({
    v4: jest.fn(() => id),
}));

describe('CreateDeviceOperation', () => {
    const createDeviceOperation = new CreateDeviceOperation;
    let repository: InMemoryRepository = new InMemoryRepository();


    it('creates device', async () => {
        const deviceRequest: DeviceCreateRequest = {
            data: {
                type: "devices",
                attributes: {
                    label: 'fiskaly',
                    algorithm: 'ECA'
                }
            }
        }

        await createDeviceOperation.execute(deviceRequest);
        const createdDevice = await repository.find();
        expect(createdDevice.id).toEqual(id);
    });
});