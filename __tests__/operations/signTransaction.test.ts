import { SignTransactionRequest } from "../../src/api/v1/schemas/requests/signTransaction";
import { SignTransactionResponse } from "../../src/api/v1/schemas/responses/signTransaction";
import Device from "../../src/models/device";
import signatureInMemory from "../../src/repositories/signature/inMemoryRepository";
import deviceInMemory from "../../src/repositories/device/inMemoryRepository";
import signTransactionOperation from "../../src/operations/signTransactionOperation";



describe('signTransactionOperation', () => {
    const operation = new signTransactionOperation();
    const signatureRepository = new signatureInMemory();
    const deviceRepository = new deviceInMemory();
    let resultedSignatureData: SignTransactionResponse;
    let signatureCounter: number = 5;
    let createdDevice: Device = {
        id: "test",
        algorithm: 'ECA',
        public_key: "test",
        private_key: "test",
        signature_counter: signatureCounter
    };
    let lastSignature: string = "last signature"
    const dataToSign: SignTransactionRequest = {
        data: "random_data"
    }

    describe("when there is a device created", () => {
        beforeEach(() => {
            jest.spyOn(signatureInMemory.prototype, 'findLast').mockResolvedValue(lastSignature)
            jest.spyOn(deviceInMemory.prototype, 'find').mockResolvedValue(createdDevice)
            jest.spyOn(deviceInMemory.prototype, 'incrementCounter').mockImplementation(() =>
                    Promise.resolve({...createdDevice, signature_counter: signatureCounter + 1})
            );
        })

        it("returns an object containing the signature", async() => {
            resultedSignatureData = await operation.execute(dataToSign);
            expect(resultedSignatureData.signature).toBeDefined();
        })

        it("returns an object containing the signed data in specific format", async() => {
            resultedSignatureData = await operation.execute(dataToSign);
            expect(resultedSignatureData.signed_data).toEqual(
                `${signatureCounter+1}_${dataToSign.data}_${lastSignature}`
            )
        })


        describe("when the signature counter in the device is zero", () => {
            beforeEach(() => {
                lastSignature = Buffer.from(createdDevice.id).toString('base64');
                signatureCounter = 0;
                jest.spyOn(deviceInMemory.prototype, 'find').mockResolvedValue(
                    {...createdDevice, signature_counter: signatureCounter}
                )
            })

            it("uses device id in the encoded_last_signature in the signed data", async() => {
                resultedSignatureData = await operation.execute(dataToSign);
                expect(resultedSignatureData.signed_data).toEqual(`${signatureCounter+1}_${dataToSign.data}_${lastSignature}`)
            })
        })

    })


    describe("when no device found", () => {
        it("raises device not found exception", () => {

        })
    })
})