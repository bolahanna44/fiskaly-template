import repositoryInterface from "../signature/repositoryInterface";
import path from "path";
import fs from "fs";
import {NotFoundError} from "../../api/errors";

class inMemoryRepository implements repositoryInterface {
    async findLast(): Promise<string> {
        const filePath = path.resolve(__dirname, `./signature.txt`);
        try {
            const data = fs.readFileSync(filePath, 'utf-8');
            return data;

        } catch {
            throw new NotFoundError('not found');
        }

    }

    async create(signature: string): Promise<string> {
        const filePath = path.resolve(__dirname, `./signature.txt`);
        fs.writeFileSync(filePath, signature);
        return signature;
    }
}

export default inMemoryRepository;