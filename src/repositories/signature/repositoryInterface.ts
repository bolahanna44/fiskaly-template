
export default interface repositoryInterface {
    findLast(): Promise<string>;
    create(signature: string): Promise<string>;
}