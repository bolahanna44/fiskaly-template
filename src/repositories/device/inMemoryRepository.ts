import repositoryInterface from "./repositoryInterface";
import Device from "../../models/device";
import fs from 'fs';
import path from 'path';
import {NotFoundError} from "../../api/errors";
class inMemoryRepository implements repositoryInterface {

    async find(): Promise<Device> {
        const filePath = path.resolve(__dirname, `./device.json`);
        try {
            const data = fs.readFileSync(filePath, 'utf-8');
            return JSON.parse(data);

        } catch {
            throw new NotFoundError('not found');
        }

    }

    async create(device: Device): Promise<Device> {
        const filePath = path.resolve(__dirname, `./device.json`);
        fs.writeFileSync(filePath, JSON.stringify(device));
        return device;
    }

    async incrementCounter(): Promise<Device> {
        // ideally the update counter should also take the device id, but here am assuming we create one device per node
        try {
            const filePath = path.resolve(__dirname, `./device.json`);
            const data = fs.readFileSync(filePath, 'utf-8');
            let device: Device = JSON.parse(data);
            device.signature_counter+=1
            fs.writeFileSync(filePath, JSON.stringify(device));
            return device
        } catch {
            throw new NotFoundError('not found');
        }
    }
}

export default inMemoryRepository;
