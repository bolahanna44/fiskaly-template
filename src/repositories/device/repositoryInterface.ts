import Device from '../../models/device';

export default interface repositoryInterface {
    find(id: string): Promise<Device>;
    create(device: Device): Promise<Device>;
    incrementCounter(): Promise<Device>;
}