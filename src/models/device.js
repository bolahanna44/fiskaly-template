"use strict";
exports.__esModule = true;
var Device = /** @class */ (function () {
    function Device(id, algorithm, signature_counter, label) {
        this.id = id;
        this.algorithm = algorithm;
        this.signature_counter = signature_counter;
        this.label = label;
    }
    return Device;
}());
exports["default"] = Device;
