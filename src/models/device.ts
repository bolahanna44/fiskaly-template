export default class Device {
    id: string;
    algorithm: 'ECA' | 'RSA';
    public_key: string;
    private_key: string;
    signature_counter: number;
    label?: string | undefined;

    constructor(
        id: string,
        algorithm: 'ECA' | 'RSA',
        signature_counter: number,
        public_key: string,
        private_key: string,
        label?: string,
    ) {
        this.id = id;
        this.algorithm = algorithm;
        this.signature_counter = signature_counter;
        this.label = label;
        this.public_key = public_key;
        this.private_key = private_key;
    }
}