import express from 'express';
import bodyParser from 'body-parser';
import v1 from './v1';

const server = express();



server.use(bodyParser.json());
server.use('/api/v1', v1);

server.get('/health', (request, response) => {
  response.status(200);
  response.send(JSON.stringify({
    status: 'pass',
    version: 'v1'
  }));
});

export default server;
