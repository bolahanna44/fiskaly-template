import express from 'express';
import devicesRouter from './devicesRouter';

const index = express.Router();

index.use('/devices', devicesRouter);

export default index;