import { JSONSchemaType } from "ajv";

export interface SignTransactionResponse {
    signature: string;
    signed_data: string;
}

const SignTransactionResponseSchema: JSONSchemaType<SignTransactionResponse> = {
    type: "object",
    properties: {
        signature: { type: "string" },
        signed_data: { type: "string" },
        },
    required: ["signature", "signed_data"],
    additionalProperties: false,
};

export default SignTransactionResponseSchema;