import { JSONSchemaType } from "ajv";

export interface SignTransactionRequest {
    data: string;
}

const SignTransactionRequestSchema: JSONSchemaType<SignTransactionRequest> = {
    type: "object",
    properties: {
        data: { type: "string" },
    },
    required: ["data"],
    additionalProperties: false,
};

export default SignTransactionRequestSchema;