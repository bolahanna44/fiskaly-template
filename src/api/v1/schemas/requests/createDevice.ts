import { JSONSchemaType } from "ajv";

export interface DeviceCreateRequest {
    data: {
        type: "devices";
        attributes?: {
            label?: string,
            algorithm: 'ECA' | 'RSA'
        };
    };
}

const deviceCreateRequestSchema: JSONSchemaType<DeviceCreateRequest> = {
    type: "object",
    properties: {
        data: {
            type: "object",
            properties: {
                type: { type: "string", const: "devices" },
                attributes: {
                    type: "object",
                    nullable: true,
                    properties: {
                        label: { type: "string", nullable: true },
                        algorithm: { type: "string", enum: ["ECA", "RSA"] },
                    },
                    required: ["algorithm"],
                    additionalProperties: false,
                },
            },
            required: ["type"],
            additionalProperties: false,
        },
    },
    required: ["data"],
    additionalProperties: false,
};

export default deviceCreateRequestSchema;