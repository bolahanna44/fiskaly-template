import express from 'express';
import Ajv from 'ajv';
import deviceCreateRequestSchema from './schemas/requests/createDevice'
import createDeviceOperation from "../../operations/createDeviceOperation";
import SignTransactionRequestSchema from "./schemas/requests/signTransaction";
import {SignTransactionResponse} from "./schemas/responses/signTransaction";
import SignTransactionOperation from "../../operations/signTransactionOperation";

const ajv = new Ajv()
const devicesRouter = express.Router();

devicesRouter.post('/', async(req, res) => {

    // TODO: we should use pessimistic locking here in case multiple devices using the same device id to sign transaction
    // TODO: the validate request schema can be moved to a middleware to avoid duplication
    // TODO: catch notfound error exceptions in middleware and send 404 status code

    const isValidRequest = ajv.validate(deviceCreateRequestSchema, req.body);
    if (!isValidRequest) {
        res.status(400).send(JSON.stringify({ errors: ajv.errors }));
    } else {
        const operation = new createDeviceOperation;
        const createdDeviceResponse = await operation.execute(req.body)
        res.status(201).send(createdDeviceResponse);
    }
});

devicesRouter.post('/sign_transaction', async(req, res) => {
    const isValidRequest = ajv.validate(SignTransactionRequestSchema, req.body);
    if (!isValidRequest) {
        res.status(400).send(JSON.stringify({ errors: ajv.errors }));
    } else {
        const operation = new SignTransactionOperation;
        const response: SignTransactionResponse = await operation.execute(req.body)
        res.status(200).send(response);
    }

});

export default devicesRouter;