import { Serializer } from "jsonapi-serializer";

const deviceSerializer = new Serializer('devices', {
    attributes: ['algorithm', 'label']
});

export default deviceSerializer;