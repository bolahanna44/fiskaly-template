import Device from "../models/device";
import inMemoryRepository from "../repositories/device/inMemoryRepository";
import deviceDeserialize from "../deserializers/deviceDeserializer";
import { DeviceCreateRequest } from "../api/v1/schemas/requests/createDevice";
import deviceSerializer from "../serializers/deviceSerializer";
import {v4 as uuidv4} from "uuid";

class createDeviceOperation {
    private deviceRepository: inMemoryRepository;

    constructor(deviceRepository: inMemoryRepository = new inMemoryRepository()) {
        this.deviceRepository = deviceRepository;
    }

    public async execute(deviceRequest: DeviceCreateRequest) {
        const deserializedDevice = await deviceDeserialize.deserialize(deviceRequest)
        const device = await this.createdDevice(deserializedDevice)
        return deviceSerializer.serialize(device)
    }

    private async createdDevice(deserializedDevice: any): Promise<Device> {
        return await this.deviceRepository.create({
            id: uuidv4(),
            label: deserializedDevice.label,
            signature_counter: 0,
            public_key: uuidv4(),
            private_key: uuidv4(),
            algorithm: deserializedDevice.algorithm
        })
    }
}

export default createDeviceOperation;
