import inMemoryDeviceRepository from "../repositories/device/inMemoryRepository";
import inMemorySignatureRepository from "../repositories/signature/inMemoryRepository";
import ECA from "../utils/ecdsa";
import RSA from "../utils/rsa";
import {SignTransactionRequest} from "../api/v1/schemas/requests/signTransaction";
import {SignTransactionResponse} from "../api/v1/schemas/responses/signTransaction";
import Device from "../models/device";

class signTransactionOperation {
    private readonly signAlgorithmResolver: any = {
        'ECA': new ECA,
        'RSA': new RSA
    }
    private deviceRepository: inMemoryDeviceRepository;
    private signatureRepository: inMemorySignatureRepository;

    constructor(
        deviceRepository: inMemoryDeviceRepository = new inMemoryDeviceRepository(),
        signatureRepository: inMemorySignatureRepository = new inMemorySignatureRepository()
    ) {
        this.deviceRepository = deviceRepository;
        this.signatureRepository = signatureRepository;
    }

    public async execute(dataToSign: SignTransactionRequest): Promise<SignTransactionResponse> {
        // for this coding challenge only i will always load the last created device
        let device = await this.deviceRepository.find();
        // @ts-ignore
        const lastSignature = await this.getLastSignature(device)
        let signature = this.signAlgorithmResolver[device.algorithm].sign(
            dataToSign.data, { public: device.public_key, private: device.private_key}
        );
        await this.signatureRepository.create(signature)
        device = await this.deviceRepository.incrementCounter();
        return {
            signature: signature,
            signed_data: `${device.signature_counter}_${dataToSign.data}_${lastSignature}`
        }
    }

    private async getLastSignature(device: Device): Promise<string> {
        if (device.signature_counter == 0) {
            return Promise.resolve(Buffer.from(device.id).toString('base64'))
        } else {
            // @ts-ignore
            return await this.signatureRepository.findLast();
        }
    }
}

export default signTransactionOperation;
